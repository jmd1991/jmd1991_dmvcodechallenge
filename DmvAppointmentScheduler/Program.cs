﻿using System;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace DmvAppointmentScheduler
{
    class Program
    {
        public static Random random = new Random();
        public static List<Appointment> appointmentList = new List<Appointment>();
        public static List<Teller> noSpecialtyTellers = new List<Teller>();
        static void Main(string[] args)
        {
            CustomerList customers = ReadCustomerData();
            TellerList tellers = ReadTellerData();
            Calculation(customers, tellers);
            OutputTotalLengthToConsole();

        }
        private static CustomerList ReadCustomerData()
        {
            string fileName = "CustomerData.json";
            string path = Path.Combine(Environment.CurrentDirectory, @"InputData\", fileName);
            string jsonString = File.ReadAllText(path);
            CustomerList customerData = JsonConvert.DeserializeObject<CustomerList>(jsonString);
            return customerData;

        }
        private static TellerList ReadTellerData()
        {
            string fileName = "TellerData.json";
            string path = Path.Combine(Environment.CurrentDirectory, @"InputData\", fileName);
            string jsonString = File.ReadAllText(path);
            TellerList tellerData = JsonConvert.DeserializeObject<TellerList>(jsonString);
            return tellerData;

        }
        static void Calculation(CustomerList customers, TellerList tellers)
        {
            // Your code goes here .....
            // Re-write this method to be more efficient instead of a assigning all customers to the same teller
            foreach (Teller teller in tellers.Teller) //Create a list of tellers without a specialty
            {
                if (teller.specialtyType == "0")
                {
                    noSpecialtyTellers.Add(teller);
                }
            }

            foreach (Customer customer in customers.Customer)
            {
                string bestMatchingTellerID = FindBestMatchingTeller(customer, tellers);
                string bestNoSpecialtyTellerID = FindBestNoSpecialtyTeller(customer, tellers);
                Teller bestMatchingTeller = tellers.Teller.Find(x => x.id == bestMatchingTellerID);
                Teller bestNoSpecialtyTeller = tellers.Teller.Find(x => x.id == bestNoSpecialtyTellerID);

                Teller chosenTeller = GetBestFitTeller(bestMatchingTeller, bestNoSpecialtyTeller, customer, tellers);

                var appointment = new Appointment(customer, chosenTeller);

                foreach (Teller teller in tellers.Teller) //Find appointed teller in Main List
                {
                    if (teller.id == chosenTeller.id)
                    {
                        teller.workload += appointment.duration;
                        break;
                    }
                }
                appointmentList.Add(appointment);
            }
        }
        static Teller GetBestFitTeller(Teller bestMatchingTeller, Teller bestNoSpecialtyTeller, Customer customer, TellerList tellers)
        {
            if (bestMatchingTeller != null) // Would have preferred to use a nullable reference type instead but I didn't have C# 8.0+ on my current machine
            {
                if (bestMatchingTeller.workload <= bestNoSpecialtyTeller.workload)
                {
                    return bestMatchingTeller;
                }
                else
                {
                    string bestOfTheRestTellerID = FindBestOfTheRestTeller(customer, tellers);
                    Teller bestOfTheRestTeller = tellers.Teller.Find(x => x.id == bestOfTheRestTellerID);

                    if (bestOfTheRestTeller != null)
                    {
                        if (bestOfTheRestTeller.workload <= bestNoSpecialtyTeller.workload)
                        {
                            return bestOfTheRestTeller;
                        }
                    }
                }
            }
            else
            {
                string bestOfTheRestTellerID = FindBestOfTheRestTeller(customer, tellers);
                Teller bestOfTheRestTeller = tellers.Teller.Find(x => x.id == bestOfTheRestTellerID);

                if (bestOfTheRestTeller != null)
                {
                    if (bestOfTheRestTeller.workload <= bestNoSpecialtyTeller.workload)
                    {
                        return bestOfTheRestTeller;
                    }
                }
            }

            return bestNoSpecialtyTeller;
        }

        static string FindBestMatchingTeller(Customer customer, TellerList tellers)
        {
            List<Teller> matchingTellers = new List<Teller>(); //Create a list of matching tellers to customers type
            Teller bestTeller = null;

            foreach (Teller teller in tellers.Teller)
            {
                if (teller.specialtyType == customer.type)
                {
                    matchingTellers.Add(teller);
                }
            }

            if (matchingTellers.Count == 0)
            {
                return null;
            }

            var orderedList = matchingTellers.OrderBy(i => i.workload);
            double leastWorkload = orderedList.LastOrDefault().workload;

            foreach (Teller teller in orderedList)
            {
                if (teller.workload == 0)
                {
                    return teller.id;
                }

                var matchingAppointment = new Appointment(customer, teller);
                var tellerWorkload = teller.workload + matchingAppointment.duration;

                if (tellerWorkload < leastWorkload)
                {
                    leastWorkload = tellerWorkload;
                    bestTeller = teller;
                }
                else
                {
                    bestTeller = orderedList.FirstOrDefault();
                }
            }

            return bestTeller.id;
        } //Find the matchingType teller that can take care of the customer the soonest

        static string FindBestNoSpecialtyTeller(Customer customer, TellerList tellers)
        {
            var orderedList = noSpecialtyTellers.OrderBy(i => i.workload);

            var appointment = new Appointment(customer, orderedList.FirstOrDefault());

            return orderedList.FirstOrDefault().id;
        } //Find the least busy NoSpecialtyTeller

        static string FindBestOfTheRestTeller(Customer customer, TellerList tellers)
        {
            List<Teller> nonMatchingTellers = new List<Teller>(); //Create a list of matching tellers to customers type
            Teller bestTeller = null;

            foreach (Teller teller in tellers.Teller)
            {
                if (teller.specialtyType != customer.type & teller.specialtyType != "0")
                {
                    nonMatchingTellers.Add(teller);
                }
            }

            if (nonMatchingTellers.Count == 0)
            {
                return null;
            }

            var orderedList = nonMatchingTellers.OrderBy(i => i.workload);
            double leastWorkload = orderedList.LastOrDefault().workload;

            foreach (Teller teller in orderedList)
            {
                if (teller.workload == 0)
                {
                    return teller.id;
                }

                var matchingAppointment = new Appointment(customer, teller);
                var tellerWorkload = teller.workload + matchingAppointment.duration;

                if (tellerWorkload < leastWorkload)
                {
                    leastWorkload = tellerWorkload;
                    bestTeller = teller;
                }
                else
                {
                    bestTeller = orderedList.FirstOrDefault();
                }
            }

            return bestTeller.id;
        } // Find the best Teller for the job that has a Specialty but does not match the appointment type

        // My reasoning for the last two Find()s is that if all other Tellers can do the job as fast as all the others (excluding the ones that specialize) then you want to
        // prioritize those that have no specialty to keep those that do have specialties open for later jobs that may need their specialty.
        static void OutputTotalLengthToConsole()
        {
            var tellerAppointments =
                from appointment in appointmentList
                group appointment by appointment.teller into tellerGroup
                select new
                {
                    teller = tellerGroup.Key,
                    totalDuration = tellerGroup.Sum(x => x.duration),
                };
            var max = tellerAppointments.OrderBy(i => i.totalDuration).LastOrDefault();
            Console.WriteLine("Teller " + max.teller.id + " will work for " + max.totalDuration + " minutes!");
        }

    }
}
